#include "Functions.cuh"

__global__ void multiplyGPU(float *matrix1, float *matrix2, float *matrixAuxRes, int size, int totalThreads)
{
	//Calculate Indexes
	int index = ((blockIdx.x * blockDim.x) + threadIdx.x);
	if (index >= totalThreads) return;

	int resultIndex = index / size;
	int offset = index % size;
	int row = resultIndex / size;
	int column = resultIndex % size;

	//Assignates the value to an auxiliar matrix to avoid race conditions
	matrixAuxRes[index] = matrix1[(row * size) + offset] * matrix2[(column * size) + offset];
}

__global__ void reduceResultMatrix(float* matrix, float* reducedMatrix, int size, int totalThreads)
{
	//Calculate Index
	int index = ((blockIdx.x * blockDim.x) + threadIdx.x);
	if (index >= totalThreads) return;

	//Initialize to 0
	reducedMatrix[index] = 0;

	for (int i = 0; i < size; ++i) reducedMatrix[index] += matrix[(index * size) + i];
}

__host__ void printMatrix(float* matrix, int size, string text)
{
	cout << text << endl;

	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			cout << matrix[(i * size) + j] << " ";
			if (j == (size - 1)) cout << endl;
		}
	}
}

__host__ void traspose(float* matrix, int size)
{
	int linearSize = size * size;
	float* destination = (float*)malloc(sizeof(float) * linearSize);

	//Trasposes the matrix into an auxiliar array
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			destination[(j * size) + i] = matrix[(i * size) + j];
		}
	}

	//Copies the trasposed matrix from the auxiliar array
	for (int i = 0; i < linearSize; ++i) matrix[i] = destination[i];

	free(destination);
}