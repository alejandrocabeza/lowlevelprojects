#include <iostream>
#include <string>

#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

using namespace std;

//Multiplies matrix1 by matrix2 and stores the value in an auxiliar matrix matrixAuxRes
__global__ void multiplyGPU(float *matrix1, float *matrix2, float *matrixAuxRes, int size, int totalThreads);

//Reduces an auxiliar result matrix (matrixAuxRes from the previous function) to a linear result array
__global__ void reduceResultMatrix(float* matrix, float* reducedMatrix, int size, int totalThreads);

//Prints a matrix preceded by the text 
__host__ void printMatrix(float* matrix, int size, string text);

//Trasposes the matrix
__host__ void traspose(float* matrix, int size);