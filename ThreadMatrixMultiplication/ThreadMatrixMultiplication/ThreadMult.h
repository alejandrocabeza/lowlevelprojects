#ifndef ThreadMult
#define ThreadMult

#include <stdio.h>
#include <time.h>

#include <malloc.h>
#include <windows.h>
#include <process.h>    /* _beginthread, _endthread */
#include <stddef.h>
#include <stdlib.h>
#include <conio.h>
#include <xmmintrin.h> //SSE

float* matrixThreadedMultiplication(float* matrix1, float* matrix2, int size);

unsigned __stdcall matrixThreadedSubMultiplication(void* arglist);

unsigned __stdcall multsumM128(void* arglist);

void traspose(float* matrix, int size);

void print(float* matrix, int size);

float* sumPartialResultMatrix(float** matrix, int size, int blocks);

#endif ThreadMult