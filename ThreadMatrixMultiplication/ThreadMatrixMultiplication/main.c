#include "ThreadMult.h"

int main()
{
	//Square matrix parameters initialization
	int size = 5;
	int linearSize = size * size;

	//Matrix memory allocation
	float* matrix1 = (float*)_aligned_malloc(sizeof(float) * linearSize, 16);
	float* matrix2 = (float*)_aligned_malloc(sizeof(float) * linearSize, 16);
	float* result;

	//Randomization of the rand seed
	srand((unsigned int)time(NULL));

	//Generation of random matrixes
	for (int i = 0; i < linearSize; ++i)
	{
		matrix1[i] = i; //(float)(rand() % 4);
		matrix2[i] = i; //(float)(rand() % 4);
	}

	//Calling the multiply function

	result = matrixThreadedMultiplication(matrix1, matrix2, size);	

	//Printing the result matrix
	print(result, size);

	//Freeing allocated memory
	_aligned_free(matrix1);
	_aligned_free(matrix2);
	free(result);

	//Avoid console exiting
	getchar();

	return 0;
}