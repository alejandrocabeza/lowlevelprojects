#include "ThreadMult.h"

//Aligned data type (128 bits)
//Can access same memory space wih different datatypes
typedef union
{
	__m128 m;
	float  f[4];
	int    i[4];
} m128;

//List of arguments for the threads
typedef struct
{
	int size;
	float* row;
	float* column;
	float* result;
} ThreadArguments;

//List of arguments for the threads initialized inside the row threads
typedef struct
{
	m128 row;
	m128 column;
	float* result;
} SubThreadArguments;

//Creates a pool of threads that will handle the multiplication of the matrixes
//This only works with square matrixes
//Matrix2 is trasposed
//One thread is created per each combination of matrix1 column and matrix2 row
//Returns the result matrix
float* matrixThreadedMultiplication(float* matrix1, float* matrix2, int size)
{
	int linearSize  = size * size;
	int blocks      = size / 4;
	int loose       = size % 4;
	int numblocks   = blocks;
	if (loose != 0) ++numblocks;

	//The second matrix is trasposed to access its values more efficiently
	traspose(matrix2, size);

	//Allocating memory for the threads and its arguments
	HANDLE* threads            = (HANDLE*)malloc(sizeof(HANDLE) * linearSize);
	ThreadArguments* arguments = (ThreadArguments*)malloc(sizeof(ThreadArguments) * linearSize);

	//Allocates memory to store the partial results of the multiplications
	float** partialResult = (float**)malloc(sizeof(float*) * linearSize);
	for (int i = 0; i < linearSize; ++i)
	{
		partialResult[i] = (float*)malloc(sizeof(float) * numblocks);
		for (int j = 0; j < numblocks; ++j)
		{
			partialResult[i][j] = 0;
		}
	}

	//Initialization of threads
	int threadID;
	for (int i = 0; i < linearSize; ++i)
	{
		int rowNum = i / size * size;
		int colNum = i % size * size;

		//Assignation of values to the arguments of the thread
		arguments[i].size   = size;
		arguments[i].row	= &(matrix1[rowNum]);
		arguments[i].column = &(matrix2[colNum]);
		arguments[i].result = partialResult[i];

		//Init of Threads
		threads[i] = (HANDLE)_beginthreadex(
			NULL,0,
			&matrixThreadedSubMultiplication,
			(void*)(&arguments[i]),
			0, &threadID);
	}

	//Waiting for threads to end
	for (int i = 0; i < linearSize; ++i)
	{
		WaitForSingleObject(threads[i], INFINITE);
	}

	//Freeing allocated memory
	free(threads);
	free(arguments);

	return sumPartialResultMatrix(partialResult, size, numblocks);
}

//Thread function
//Creates a pool of threads that will handle the multiplication of each rw and column of the matrixes
unsigned __stdcall matrixThreadedSubMultiplication(void* arglist)
{
	
	//printf("%d\n", 0);
	//Casting the arguments to the correct datatype
	ThreadArguments* arguments = (ThreadArguments*)arglist;

	//Getting number of threads needed
	int blocks      = arguments->size / 4;
	int loose       = arguments->size % 4;
	int numblocks   = blocks;
	if (loose != 0) ++numblocks;
	int linearSize  = arguments->size * arguments->size;

	if (loose >= 4) printf("More than 4 numbers loose in a block.\n");

	//Allocating memory for the threads and its arguments
	HANDLE* threads                  = (HANDLE*)malloc(sizeof(HANDLE) * numblocks);
	SubThreadArguments* subArguments = (SubThreadArguments*)malloc(sizeof(SubThreadArguments) * numblocks);
	int threadID;
	//Creation of threads to multiply the blocks
	for (int i = 0; i < numblocks; ++i)
	{
		//Checks if the block to be parsed is complete or not (has 4 values). 
		//If it's not complete (has less than 4 values) it uses a different method of assignation to avoid memory errors
		if ((i == numblocks - 1) && (loose != 0))
		{
			for (int j = 0; j < loose; ++j)
			{
				subArguments[i].row.f[j]    = arguments->row[(i * 4) + j];
				subArguments[i].column.f[j] = arguments->column[(i * 4) + j];
			}
			
			for (int j = loose; j < 4; ++j)
			{ 
				subArguments[i].row.f[j]    = 0.0f;
				subArguments[i].column.f[j] = 0.0f;
			}
		}
		else
		{
			for (int j = 0; j < 4; ++j)
			{
				subArguments[i].row.f[j]    = arguments->row[(i * 4) + j];
				subArguments[i].column.f[j] = arguments->column[(i * 4) + j];
			}
		}
 

		subArguments[i].result = &(arguments->result[i]);

		//Initialization of Threads
		threads[i] = (HANDLE)_beginthreadex(
			NULL, 0,
			&multsumM128,
			(void*)(&subArguments[i]),
			0, &threadID);
	}

	//Waiting for threads to end
	for (int i = 0; i < numblocks; ++i)
	{
		WaitForSingleObject(threads[i], INFINITE);
	}

	//Allocated memory freeing
	free(threads);
	free(subArguments);

	//Terminates the current thread
	_endthreadex(0);
	return 0;
}

//Thread function
//Multiplies two m128 variables and sums the 4 values of the resulting m128
unsigned __stdcall multsumM128(void* arglist)
{
	SubThreadArguments* arguments = (SubThreadArguments*)arglist;

	float x = 0.0f;
	m128 res;
	__m128 a = arguments->row.m;
	__m128 b = arguments->column.m;

	//Multiplies two m128 variables
	res.m = _mm_mul_ps(a, b);
	
	//Sums the 4 values of the result m128 and assignates it to an escalar float variable
	for (int i = 0; i < 4; ++i) x += res.f[i];

	//Assignates the result
	*(arguments->result) = x;

	//Terminates the current thread
	_endthreadex(0);
	return 0;
}

//Transforms the partial result matrix into a linear matrix
float* sumPartialResultMatrix(float** matrix, int size, int blocks)
{
	float* result = (float*)malloc(sizeof(float) * size * size);
	int linearsize = size * size;
	float x = 0.0f;

	for (int i = 0; i < linearsize; ++i)
	{
		x = 0.0f;

		for (int j = 0; j < blocks; ++j) x += matrix[i][j];

		result[i] = x;
	}

	return result;
}

//Trasposes the matrix
void traspose(float* matrix, int size)
{
	int linearSize = size * size;
	float* destination = (float*)malloc(sizeof(float) * linearSize);

	//Trasposes the matrix into an auxiliar array
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			destination[(j * size) + i] = matrix[(i * size) + j];
		}
	}

	//Copies the trasposed matrix from the auxiliar array
	for (int i = 0; i < linearSize; ++i) matrix[i] = destination[i];

	free(destination);
}

//Prints a linear matrix
void print(float* matrix, int size)
{
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			printf("%f ", matrix[(i * size) + j]);
			if (j == (size - 1)) printf("\n");
		}
	}
}